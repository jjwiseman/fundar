# Fundar

![iPhone screenshot](screenshot.png?raw=true "iPhone screenshot")

## Server side:

```
cd server
yarn install
export ADSBX_KEY=<your ADS-B Exchange API key>
npm run dev
```

## Client side:

On your laptop (or whatever):

Install [expo](https://expo.io/).

```
cd app
expo start
```

Edit `App.tsx` and change `<myhost>` to the IP and port of the server
that you started above:

```
  const url = `http://<myhost>/ping?id=${id}&lat=${lat}&lon=${lon}`;
```

On your iOS device:

Install the [expo
app](https://itunes.apple.com/app/apple-store/id982107779). Open it.
Give it any permissions it asks for.

Select the project in development. Give it any permissions it asks
for.
