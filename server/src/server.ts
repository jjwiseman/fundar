import express from 'express';
import fetch from 'node-fetch';
import morgan from 'morgan';
import * as geolib from 'geolib';

const app = express();
const port = 3001;

app.use(express.urlencoded({ extended: false }));
app.use(express.json());
app.use(morgan('combined'))

app.get('/', (req, res) => {
  res.send('The sedulous hyena ate the antelope!');
});

// ADS-B Exchange JSON.
type AdsbxResult = {
  now: number,
  ac: AdsbxAircraft[]
};

type AdsbxAircraft = {
  lat?: number;
  lon?: number;
  alt_baro?: number;
  alt_geom?: number;
  hex: string;
  r?: string;
  flight?: string;
  track?: number;
  gs?: number;
  seen_pos?: number;
};

type Coords = {
  latitude: number,
  longitude: number,
  altitude?: number
};

type Aircraft = {
  adsb: AdsbxAircraft,
  distance_meters?: number,
}

type TargetsResponse = {
  aircraft: Aircraft[]
  now: number
}

async function getTargetsForClient(id: string): Promise<TargetsResponse> {
  let user = userDatabase.get(id);
  if (!user) {
    throw new Error(`No user with id ${id}`);
  }
  if (!user.coords) {
    throw new Error(`No coords for user ${id}`);
  }
  let targets: TargetsResponse;
  targets = await getAdsbxTargets(user.coords.latitude, user.coords.longitude, 10);
  return targets;
}


async function getAdsbxTargets(lat: number, lon: number, radius: number): Promise<TargetsResponse> {
  let headers: { [key: string]: any } = {};
  headers['compress'] = true;
  const apiKey = process.env['ADSBX_KEY'];
  if (apiKey) {
    headers['api-auth'] = apiKey;
  }
  const resultObj = await fetch(
    `http://lockheed.local:8000/api/aircraft/v2/lat/${lat}/lon/${lon}/dist/${radius}/`,
    headers);
  const result = (await resultObj.json()) as AdsbxResult;
  console.log(`Got ${result.ac?.length} aircraft from API`);
  const targets = result.ac.map((ac) => adsbxAircraftToTarget({ latitude: lat, longitude: lon }, result.now, ac)).filter(ac => ac != null) as Aircraft[];
  return { aircraft: targets, now: result.now };
}

function adsbxAircraftToTarget(pov: Coords, now: number, ac: AdsbxAircraft): Aircraft | null {
  if (ac.lat != null && ac.lon != null && ac.seen_pos != null) {
    return {
      adsb: ac,
      distance_meters: geolib.getDistance(pov, { latitude: ac.lat, longitude: ac.lon })
    }
  } else {
    return null;
  }
}

type User = {
  id: string;
  coords: Coords | null;
  deviceToken: Coords | null;
  lastUpdateTime: number | null;
}

const userDatabase = new Map<string, User>();

userDatabase.set('1', { id: '1', coords: null, deviceToken: null, lastUpdateTime: null });


function setUserCoords(id: string, coords: Coords) {
  const user = userDatabase.get(id);
  if (!user) {
    throw new Error(`No user with id ${id}`);
  }
  user.coords = coords;
  user.lastUpdateTime = (new Date()).getTime();
}

// Fetch user info.
app.get('/user/:id', async (req, res) => {
  const id = req.params.id;
  const user = userDatabase.get(id);
  if (!user) {
    res.status(404).send(`No user with id ${id}`);
  }
  res.json(user);
});

// Associate a device token with a user.
app.post('/user/:userId/deviceToken', async (req, res) => {
  console.log('req.params:')
  console.log(req.params);
  console.log('req.body');
  console.log(req.body);
  const userId = req.params.userId;
  const deviceToken = req.body.deviceToken;
  if (userId && deviceToken) {
    let user = userDatabase.get(userId);
    if (!user) {
      // For now, just create a new user.
      user = { id: userId, deviceToken, lastUpdateTime: null, coords: null };
      userDatabase.set(userId, user);
    } else {
      user.deviceToken = deviceToken;
    }
    console.log(`User: ${userId}: Added deviceToken ${deviceToken}`);
  } else {
    res.status(400).send('Missing required parameters');
    return;
  }
  res.send('OK')
});

// Update the location of a user.
app.post('/user/:userId/location', async (req, res) => {
  const userId = req.params.userId;
  const latitude = req.body.latitude;
  const longitude = req.body.longitude;
  if (!userId || !latitude || !longitude) {
    res.status(400).send('Missing required parameters');
    return
  }
  setUserCoords(userId, { latitude, longitude });
  res.send('OK');
});

// Get aircraft near user.
app.get('/targets', async (req, res) => {
  const latStr = req.query.lat as string;
  const lonStr = req.query.lon as string;
  const userId = req.query.userId as string;
  console.log(`client: ${userId} ${latStr} ${lonStr}`);
  if (!latStr || !lonStr || !userId) {
    res.status(400).send('Missing required parameters');
    return;
  }
  const latitude = parseFloat(latStr);
  const longitude = parseFloat(lonStr);
  setUserCoords(userId, { latitude, longitude, altitude: 0 });
  const response = await getTargetsForClient(userId);
  res.json(response);
});

app.get('/aircraft', async (req, res) => {
  let headers: { [key: string]: any } = {};
  headers['compress'] = true;
  const apiKey = process.env['ADSBX_KEY'];
  if (apiKey) {
    headers['api-auth'] = apiKey;
  }
  const resultObj = await fetch(
    `http://lockheed.local:8000/api/aircraft/v2/all`,
    headers);
  const result = (await resultObj.json()) as AdsbxResult;
  console.log(`Got ${result.ac?.length} aircraft from API`);
  if (req.query.hexes) {
    // Only keep aircraft with the given hex codes.
    const filterHexes = new Set<string>((req.query.hexes as string).split(','));
    result.ac = result.ac.filter(ac => filterHexes.has(ac.hex));
  }
  // Only keep


});

app.listen(port, '0.0.0.0', err => {
  if (err) {
    return console.error(err);
  }
  return console.log(`server is listening on ${port}`);
});


async function checkForIncoming() {
  console.log("Background scan.")
  // For each user, check whether any targets are within 1 mile.
  for (const user of userDatabase.values()) {
    if (user.deviceToken && user.coords) {
      const targets = await getTargetsForClient(user.id);
      for (const target of targets.aircraft) {
        if (target.distance_meters && target.distance_meters < 1609.34) {
          console.log(`User ${user.id} is near target ${target.adsb.hex}`);
          // sendNotification(user.deviceToken, `Nearby aircraft: ${target.adsb.hex}`);
        }
      }
    }
  }
  
}

setInterval(checkForIncoming, 30000);