import React, { useState, useEffect } from 'react';
import { Image, Platform, Text, View, StyleSheet, GetPhotosParamType } from 'react-native';
import Constants from 'expo-constants';
import * as Location from 'expo-location';
import { Notifications } from 'expo';
import * as Permissions from 'expo-permissions';
import * as TaskManager from 'expo-task-manager';
import { AsyncStorage, Slider, StatusBar } from 'react-native';
import * as geolib from 'geolib';
import { GeolibInputCoordinatesWithTime } from 'geolib/es/types';
import { SafeAreaProvider } from 'react-native-safe-area-context';

type IndicatorArrowProps = {
  heading: number;
};

function IndicatorArrow(props: any): JSX.Element {
  let heading = props.heading || 0;
  return <Image
    source={require('./assets/arrow.png')}
    style={{
      alignItems: 'center',
      marginTop: 50,
      width: 100,
      height: 250,
      transform: [{ rotate: (-heading.toFixed(0)) + 'deg' }]
    }}
  />
}

async function savePushToken(token: string) {
  AsyncStorage.setItem('pushToken', token);
}

async function getSavedPushToken(): Promise<string | null> {
  return AsyncStorage.getItem('pushToken');
}

type Coords = {
  latitude: number,
  longitude: number,
  altitude: number
};

type Target = {
  coords: Coords;
  heading: number;
  speed: number;
  label: string;
  datum: number;
}

type Vector = {
  bearing: number,
  elevation: number
};

function toDegrees(radians: number): number {
  return radians * 180 / Math.PI;
}

type TargetVector = {
  bearing: number,
  distance: number,
  elevation: number
};

function vectorToTarget(origin: Coords, dest: Coords): TargetVector {
  const bearing = geolib.getRhumbLineBearing(origin, dest);
  const distance = geolib.getDistance(origin, dest);
  const elevation = toDegrees(Math.atan2(dest.altitude - origin.altitude, distance));
  return { bearing, distance, elevation };
}

const home: Target = {
  coords: {
    latitude: 34.13361341687469,
    longitude: -118.19229904033807,
    altitude: 220
  },
  speed: 0,
  heading: 0,
  datum: 0,
  label: 'home'
};

type Subscription = {
  remove: () => void;
};

type GeolocationData = {
  locations: Location.LocationData[];
  error: string | null;
};

type GeolocationBackgroundTask = (geolocData: GeolocationData) => void;

function predictTarget(target: Target, compensation: number): Target {
  const now = (new Date()).getTime();
  const timeDeltaSecs = (now - target.datum) / 1000 + compensation;
  const distance = target.speed * timeDeltaSecs;
  const estimatedPos = geolib.computeDestinationPoint(target.coords, distance, target.heading);
  const newCoords = {
    ...estimatedPos,
    altitude: target.coords.altitude
  };
  return {
    ...target,
    coords: newCoords
  };
}

async function ping(coords: Coords, id: string) {
  const lat = coords.latitude;
  const lon = coords.longitude;
  const url = `http://heavymeta.org:3001/ping?id=${id}&lat=${lat}&lon=${lon}`;
  const response = await fetch(url);
  const data = response.json();
  return data;
}

function useLocation(options: Location.LocationOptions): [Location.LocationData | null, Location.HeadingData | null, string | null] {
  const [gotLocationPerm, setGotLocationPerm] = useState(false);
  const [location, setLocation] = useState<Location.LocationData | null>(null);
  const [heading, setHeading] = useState<Location.HeadingData | null>(null);
  const [errorMessage, setErrorMessage] = useState<string | null>(null);
  const [headingSubscription, setHeadingSubscription] = useState<Subscription | null>(null);
  const [locationSubscription, setLocationSubscription] = useState<Subscription | null>(null);
  (async () => {
    if (!gotLocationPerm) {
      // Get location permissions.
      setGotLocationPerm(true);
      let permission = await Location.requestPermissionsAsync();
      if (permission.status !== 'granted') {
        setErrorMessage('Permission to access location was denied');
      } else if (permission.ios && permission.ios.scope != 'always') {
        setErrorMessage('Need to give permission to always access location.');
      }
      // Subscribe to heading and location updates.
      console.log('Subscribing to heading');
      setHeadingSubscription(await Location.watchHeadingAsync(setHeading));
      console.log('Subscribing to location');
      setLocationSubscription(await Location.watchPositionAsync(options, (loc) => {
        console.log(`Got new location ${JSON.stringify(loc.coords)}`);
        setLocation(loc);
      }));
    }
  })();
  useEffect(() => {
    return () => {
      headingSubscription?.remove();
      locationSubscription?.remove();
    };
  }, []);
  return [location, heading, errorMessage];
}

type UseTargetsOptions = {
  minInterval?: number,
  now?: number
};

function makeTargetEffect(id: string | null, location: Location.LocationData | null, setTarget: React.Dispatch<React.SetStateAction<Target | null>>, options: UseTargetsOptions = {}) {
  const [lastPingTime, setLastPingTime] = useState<number>();
  return () => {
    if (id && location) {
      const minInterval = options.minInterval || 10000;
      const now = options.now || (new Date()).getTime();
      (async () => {
        if (!lastPingTime || now - lastPingTime > minInterval) {
          console.log('Pinging');
          const lat = location.coords.latitude;
          const lon = location.coords.longitude;
          const url = `http://heavymeta.org:3001/ping?id=${id}&lat=${lat}&lon=${lon}`;
          const response = await fetch(url);
          const responseData = await response.json();
          if (responseData.targets && responseData.targets.length > 0) {
            console.log('Setting target', responseData.targets[0]);
            setTarget(responseData.targets[0]);
          } else {
            console.log('No targets');
            setTarget(null);
          }
        }
      })();
    }
  }
}

export default function App() {
  const locationOptions = {
    accuracy: Location.Accuracy.BestForNavigation
  };
  const [location, heading, locationErrorMessage] = useLocation(locationOptions);
  // console.log('LOC ' + JSON.stringify(location));
  const [predictionComp, setPredictionComp] = useState<number>(10);
  const [target, setTarget] = useState<Target | null>(null);
  const [expoPushToken, setExpoPushToken] = useState<string | null>(null);
  const nowGranular = ((new Date()).getTime() / 60000).toFixed(0);
  const [errorMsg, setErrorMsg] = useState<string | null>(null);
  const [gotLocationPerm, setGotLocationPerm] = useState<boolean>(false);
  const [gotNotificationPerm, setGotNotifcationPerm] = useState<boolean>(false);
  const [notification, setNotification] = useState<Notification | null>(null);
  const [now, setNow] = useState<Date>(new Date());
  const [myInterval, setMyInterval] = useState<any>(null);

  useEffect(makeTargetEffect(expoPushToken, location, setTarget), [location, expoPushToken, nowGranular]);

  if (!myInterval) {
    setMyInterval(setInterval(() => setNow(new Date()), 250));
  }

  // const handleLocation = (location: Location.LocationData, isBackground: boolean) => {
  //   setLocation(location.coords);
  //   const maybePing = async () => {
  //     const lastPingTimeStr = await AsyncStorage.getItem('lastPingTime');
  //     const lastPingTime = lastPingTimeStr ? JSON.parse(lastPingTimeStr) : null;
  //     const timeSinceLastPing = lastPingTime ? location.timestamp - lastPingTime : undefined;
  //     console.log('location: time since last ping:', timeSinceLastPing);
  //     if (!timeSinceLastPing || timeSinceLastPing > 60000) {
  //       console.log('Sending location!');
  //       AsyncStorage.setItem('lastPingTime', JSON.stringify(location.timestamp));
  //       const lat = location.coords.latitude;
  //       const lon = location.coords.longitude;
  //       const token = await getSavedPushToken();
  //       if (!token) {
  //         console.log('WTF no token');
  //         return;
  //       }
  //       const result = await ping(location.coords, token);
  //       if (result.targets && result.targets.length > 0) {
  //         console.log('setting target', result.targets[0]);
  //         setTarget(result.targets[0]);
  //       }
  //     }
  //   };
  //   maybePing();
  // };

  // Receive location updates in the background.
  // useEffect(() => {
  //   const geolocationTask: TaskManager.TaskManagerTaskExecutor = (body) => {
  //     if (body.error) {
  //       console.log("Error", body.error);
  //     } else {
  //       const data = body.data as GeolocationData;
  //       handleLocation(data.locations[0], true);
  //     }
  //   };

  //   (async () => {
  //     TaskManager.defineTask('geolocation', geolocationTask);
  //     Location.startLocationUpdatesAsync(
  //       'geolocation',
  //       { accuracy: Location.Accuracy.Low });
  //     return () => Location.stopLocationUpdatesAsync('geolocation');
  //   })();
  // }, []);

  // Handles push notifications.
  const handlePushNotification = (notif: Notification) => {
    setNotification(notif);
    console.log(notif);
  };

  // Get push notification permissions and install handler.
  useEffect(() => {
    (async () => {
      if (!gotNotificationPerm) {
        setGotNotifcationPerm(true);
        if (Constants.isDevice) {
          const { status: existingStatus } = await Permissions.getAsync(Permissions.NOTIFICATIONS);
          let finalStatus = existingStatus;
          if (existingStatus !== 'granted') {
            const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
            finalStatus = status;
          }
          if (finalStatus !== 'granted') {
            setErrorMsg('Failed to get push token for push notification!');
          } else {
            const token = await Notifications.getExpoPushTokenAsync();
            console.log(token);
            setExpoPushToken(token);
            savePushToken(token);
            Notifications.addListener(handlePushNotification as any);
          }
        } else {
          setErrorMsg("Must run on device.");
        }
      }
    })();
  }, []);

  let text = '';
  if (heading) {
    text += heading.trueHeading.toFixed(1);
  }


  let notificationText = '';
  if (notification) {
    notificationText = JSON.stringify(notification, null, 2);
  }

  const predictedTarget = location && target ? predictTarget(target, predictionComp) : null;
  const vector = location && predictedTarget ? vectorToTarget(location.coords, predictedTarget.coords) : null;
  let arrowHeading = 0;
  if (heading && vector) {
    arrowHeading = heading.trueHeading - vector.bearing;
  }

  return (
    <View style={styles.container}>
      <StatusBar backgroundColor='white' barStyle="dark-content" />
      <Text style={styles.error}>{errorMsg}</Text>
      <Text style={styles.paragraph}>{text}</Text>
      <View style={{ alignItems: 'center' }}>
        <IndicatorArrow heading={arrowHeading} />
      </View>
      {location ? (
        <Text style={styles.paragraph}>
          Your location: {'\n'}
          {location.coords.latitude.toFixed(3)} {location.coords.longitude.toFixed(3)} Altitude: {location.coords.altitude.toFixed(0)} m
          {'\n'}
        </Text>
      ) : null}
      <Text style={styles.paragraph}>
        Target: {predictedTarget?.label} {'\n'}
        {predictedTarget?.coords.latitude.toFixed(3)} {predictedTarget?.coords.longitude.toFixed(3)} Altitude {predictedTarget?.coords.altitude.toFixed(0)} m{'\n'}
        Range {vector ? (Math.round(vector.distance / 10) * 10).toFixed(0) : ""} m, bearing {vector?.bearing.toFixed(0)}° {'\n'}
        Speed {target?.speed?.toFixed(0)} m/s{'\n'}
        Elevation {vector?.elevation.toFixed(0)}°
      </Text>
      <Text style={styles.error}>{notificationText}</Text>
      <Text>Prediction compensation: {predictionComp.toFixed(1)} secs</Text>
      <Slider
        minimumValue={0}
        maximumValue={30}
        value={predictionComp}
        onValueChange={val => setPredictionComp(val)}
      />
      <Text>Datum age: {predictedTarget ? ((now.getTime() - predictedTarget.datum) / 1000).toFixed(0) : ""} </Text>
      <Text></Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 50,
  },
  error: {
    textAlign: 'center',
    color: 'red'
  },
  paragraph: {
    color: 'green',
    textAlign: 'center'
  },
  buttonContainer: {
    flexDirection: 'row',
    alignItems: 'stretch',
    marginTop: 15,
  },
  button: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#eee',
    padding: 10,
  },
  middleButton: {
    borderLeftWidth: 1,
    borderRightWidth: 1,
    borderColor: '#ccc',
  },
  sensor: {
    marginTop: 15,
    paddingHorizontal: 10,
  }
});
